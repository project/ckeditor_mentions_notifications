<?php

namespace Drupal\ckeditor_mentions_notifications\EventSubscriber;

use Drupal\comment\Entity\Comment;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\ckeditor_mentions\Events\CKEditorEvents;
use Drupal\ckeditor_mentions\Events\CKEditorMentionsEvent;
use Drupal\Core\Utility\Token;
use Drupal\user\Entity\User;
use Drupal\user\UserDataInterface;

/**
 * Class MentionEventsSubscriber.
 *
 * @package Drupal\custom_events\EventSubscriber
 */
class NotificationMentionEventsSubscriber implements EventSubscriberInterface {

  /**
   * The user data service.
   *
   * @var UserDataInterface
   */
  protected UserDataInterface $userData;

  /**
   * The mail manager service.
   *
   * @var MailManagerInterface
   */
  protected MailManagerInterface $mailManager;

  /**
   * The Token service
   *
   * @var Token
   */
  protected Token $tokenService;

  /**
   * The Configuration factory manager service
   *
   * @var ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The Logger factory service
   *
   * @var LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  public function __construct(UserDataInterface $user_data, MailManagerInterface $mail_manager, ConfigFactoryInterface $config_factory, Token $token_service, LoggerChannelFactory $logger) {
    $this->userData = $user_data;
    $this->mailManager = $mail_manager;
    $this->configFactory = $config_factory;
    $this->tokenService = $token_service;
    $this->logger = $logger->get("ckeditor_mentions_notifications");
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[CKEditorEvents::MENTION_FIRST][] = ['initiateNotification', 0];
    return $events;
  }

  /**
   * @param CKEditorMentionsEvent $event
   *
   * @return void
   */
  public function initiateNotification(CKEditorMentionsEvent $event) { //TODO: Move this to a service of it's own
    $mentionedEntity = $event->getMentionedEntity();
    if ($mentionedEntity instanceof User) {

      $uid = $mentionedEntity->id();
      $userPref = $this->userData->get('ckeditor_mentions_notifications', $uid, 'mentions_notifications_settings_key');

      if ($userPref === "Enable") {
        $entity = $event->getEntity();
        if ($entity instanceof Node || $entity instanceof Comment) {
          $data = [];
          $data['entity'] = $entity;
          $config = $this->configFactory->get('ckeditor_mentions_notification.settings');
          $subject = $this->tokenService->replacePlain($config->get('mentions_notifications_email_subject'), $data);
          $body = $this->tokenService->replacePlain($config->get('mentions_notifications_email_body'), $data);
          $email = $mentionedEntity->getEmail();

          $params['subject'] = $subject;
          $params['message'] = $body;
          $result = $this->mailManager->mail('ckeditor_mentions_notifications', 'send_ckeditor_mentions_notifications', $email, 'en', $params, NULL, TRUE);
          if (!$result['result']) {
            $this->logger->notice("Unable to send a mentions notification out to : {$mentionedEntity->getAccountName()}");
          }
        }
      }
    }
  }
}
